
package tercer_tarea;

/**
 *
 * @author mike
 */
public class Tercer_Tarea {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here   //Creando el objeto persona 
    Doctor doc = new Doctor();
    doc.setNombres("Maycol Moises");
    doc.setApellidos("Erazo Jordan");
    doc.setNacimientof("16 de Octubre de 1965");
    doc.setSexo ("Masculino");
    doc.setArea("Cardiología");
    doc.setFacultad("FCM-UNAH");
    doc.setTiempo("6 años");
    doc.setHospital("Hospital del Valle");
    doc.setCelular(33213321);
    doc.setCorreo("erazomike30@gmail.com");

System.out.println("Nombres: " + doc.getNombres() );
System.out.println("Apellidos: " + doc.getApellidos() );
System.out.println("Fecha de nacimiento: " + doc.getNacimientof() );
System.out.println("Sexo: " + doc.getSexo() );
System.out.println("Área de especialización: " + doc.getArea() );
System.out.println("Facultad de Medicina: " + doc.getFacultad() );
System.out.println("Tiempo de ejercimiento laboral: " + doc.getTiempo() );
System.out.println("Lugar de trabajo: " + doc.getHospital() );
System.out.println("Número Celular: " + doc.getCelular() );
System.out.println("Correo: " + doc.getCorreo() );
}
public static class Doctor{

    private String nombres;
    private String apellidos;
    private String nacimiento;
    private String sexo; 
    private String facultad;
    private String area;
    private String tiempo;
    private String hospital;
    private int celular;
    private String correo;
    


    public String getNombres(){
        return nombres;
    }
    public String getApellidos(){
        return apellidos;
    }
    public String getNacimientof(){
        return nacimiento;
    }
    public String getSexo(){
        return sexo;
    }
     public String getArea(){
        return area;
    }
      public String getFacultad(){
        return facultad;
    }
      public String getTiempo(){
        return tiempo;
    }
      public String getHospital(){
        return hospital;
    }
    public int getCelular(){
        return celular;
    }
    public String getCorreo(){
        return correo;
    }

    public void setNombres(String nombres){
        this.nombres = nombres;
    }
    public void setApellidos(String apellidos){
        this.apellidos = apellidos;
    }
    public void setNacimientof(String nacimiento){
        this.nacimiento = nacimiento;
    }
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    public void setArea(String area){
        this.area = area;
    }
    public void setFacultad(String facultad){
        this.facultad = facultad;
   }
    public void setTiempo(String tiempo){
        this.tiempo = tiempo;
   }
    public void setHospital(String hospital){
        this.hospital = hospital;
   }

    public void setCelular(int celular){
        this.celular = celular;
    }
     public void setCorreo(String correo){
        this.correo = correo;
   }
}
}
   
    

